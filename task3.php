<?php

// Artsiom Mkrtychan
/* 11.01.2017 */

//6.1
echo "<h3>6.1</h3>";
$products = array(    
    array('name' => 'Телевизор', 'price' => '400', 'quantity' => 1),     
    array('name' => 'Телефон', 'price' => '300', 'quantity' => 3),     
    array('name' => 'Кроссовки', 'price' => '150', 'quantity' => 2), 
);

function getPoductsSumCount($products)
{
    
    $wholeSum = 0;
    $wholeQuantity = 0;
    
    foreach($products as $product){
        $price = getProductPrice($product);
        $quantity = getProductQuantity($product);
        
        $wholeSum += $price * $quantity;
        $wholeQuantity += $quantity;
    }    
    
    $result = [
        "wholeSum" => $wholeSum,
        "wholeQuantity" => $wholeQuantity
    ];
    
    return $result;
}


function getProductPrice($product)
{
    return $product["price"];
}

function getProductQuantity($product)
{
    return $product["quantity"];
}

function printProductInfo($productInfo)
{
    echo "<div>wholeSum {$productInfo["wholeSum"]}</div>";
    echo "<div>wholeQuantity {$productInfo["wholeQuantity"]}</div><br/>";
}


$productInfo = getPoductsSumCount($products);

echo "<h4>Initial array</h4>";
echo "<pre>";
    print_r($products);
echo "</pre>";


printProductInfo($productInfo);


//6.2
echo "<h3>6.2</h3>";

function getDiscriminant($a, $b, $c) 
{
    $result = pow($b,2) - 4 * $a * $c;

    return $result;
}

function getFirstRoot($a,$b,$d) 
{
    $result = (-$b + sqrt($d)) / 2 * $a;

    return $result;
}

function getSecondRoot($a,$b,$d) 
{
    $result = (-$b - sqrt($d)) / 2 * $a;

    return $result;
}

function getEquasionResult($a, $b, $c) 
{            
    $d = getDiscriminant($a,$b,$c);
    $result = false;
    
    if ($d > 0 ){
        $firstRoot = getFirstRoot($a,$b,$d);
        $secondRoot = getSecondRoot($a,$b,$d);

        $result = [
            "firstRoot" => $firstRoot,
            "secondRoot" => $secondRoot
        ];
    } elseif ($d == 0){
        $firstRoot = getFirstRoot($a,$b,$d);

        $result = $firstRoot;
    } else {
        $result = false;
    }
        
    return $result;
}

$oneRootResult = getEquasionResult(3, -18, 27);
$twoRootResult = getEquasionResult(1, -70, 600);
$noRootResult = getEquasionResult(1, -3, 4);

echo "<div>One root result args : 3, -18, 27</div>";
echo "<div>One root result : {$oneRootResult}</div>";
echo "<div>Two root result args : 1, -70, 600</div>";
echo "<div>Two root result : {$twoRootResult["firstRoot"]} and {$twoRootResult["secondRoot"]}</div>";
echo "<div>No root result args : 1, -3, 4</div>";
echo "<div>No root result : {$noRootResult}</div>";


//6.3
echo "<h3>6.3</h3>";

$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95); 

function deleteNegatives($digits)
{
    foreach($digits as $key => $digit){
        if($digit < 0){
            unset($digits[$key]);            
        }
    }
    
    $digits = array_values($digits);
    
    return $digits;
}

$notNegativeDigits = deleteNegatives($digits);

echo "<h4>Array with negatives:</h4>";
echo "<pre>";
    print_r($digits);
echo "</pre>";

echo "<h4>Array without negatives:</h4>";
echo "<pre>";
    print_r($notNegativeDigits);
echo "</pre>";


//6.4
echo "<h3>6.4</h3>";

$digits = array(2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95); 

function deleteNegativesOnRef(&$digits)
{
    foreach($digits as $key => $digit){
        if($digit < 0){
            unset($digits[$key]);            
        }
    }
    
    $digits = array_values($digits);        
}



echo "<h4>Array with negatives:</h4>";
echo "<pre>";
    print_r($digits);
echo "</pre>";

deleteNegativesOnRef($digits);

echo "<h4>Array without negatives:</h4>";
echo "<pre>";
    print_r($digits);
echo "</pre>";

//7.2
echo "<h3>7.2</h3>";

?>


<form action="/task3.php" method="post">
    <div>
        Please input digits
    </div>
    <div>
        <p>Digit 1: <input name="digits[]" type="text"/></p>
        <p>Digit 2: <input name="digits[]" type="text"/></p>
        <p>Digit 3: <input name="digits[]" type="text"/></p>
        <p>Digit 4: <input name="digits[]" type="text"/></p>
        <p>Digit 5: <input name="digits[]" type="text"/></p>
        <p>Digit 6: <input name="digits[]" type="text"/></p>
        <p>Digit 7: <input name="digits[]" type="text"/></p>
    </div>                

    <input type="submit" name="submit" value="submit" />
</form>
  

<?php

$digits = $_POST["digits"];

if(!empty($digits)){
    echo "<h4>Digits:</h4>";
    echo "<pre>";
        print_r($digits);
    echo "</pre>";

    $minDigit = getMinDigit($digits);
    $maxDigit = getMaxDigit($digits);
    $midDigit = getMidDigit($digits);

    echo "<div>Min digit: {$minDigit}</div>";
    echo "<div>Max digit: {$maxDigit}</div>";
    echo "<div>Mid digit: {$midDigit}</div>";
}

function getMaxDigit($digits)
{
    $maxDigit = PHP_INT_MIN;
    
    foreach($digits as $digit){
        $intDigit = intval($digit);
        
        if($intDigit > $maxDigit){
            $maxDigit = $intDigit;
        }
    }
    
    return $maxDigit;
}

function getMinDigit($digits)
{
    $minDigit = PHP_INT_MAX;
    
    foreach($digits as $digit){
        $intDigit = intval($digit);
        
        if($intDigit < $minDigit){
            $minDigit = $intDigit;
        }
    }  
    
    return $minDigit;
}

function getMidDigit($digits)
{
    $result = 0;
    
    foreach($digits as $digit){
        
        $intDigit = intval($digit);
        $result += $intDigit;   
    }      
        
    $result /= count($digits);      
    
    return $result;
}


//7.3
echo "<h3>7.3</h3>";

?>

<form action="/task3.php" method="post">
    <div>
        Please input digits
    </div>
    <div>
        <p>Name: <input name="name" type="text"/></p>
        
        <p>Man: <input type="radio" name="sex" value="m"/></p>
        <p>Woman: <input type="radio" name="sex" value="w"/></p>        
    </div>                

    <input type="submit" name="submit" value="submit" />
</form>

<?php

$name = $_POST["name"];
$sex = $_POST["sex"];

echo "Добро пожаловать, ";

if($sex == "m"){
    echo "мистер ";
} elseif ($sex == "w"){
    echo "миссис ";
}

echo "{$name}!";

?>
