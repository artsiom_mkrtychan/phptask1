<?php

    // Artsiom Mkrtychan
    /* 8.02.2017 */

    session_start();

    $_SESSION["third_form_result"] = $_GET["third_form_result"];         

    $firstResult = $_SESSION["first_form_result"] == 5 ? 'correct' : 'incorrect';
    $secondResult = $_SESSION["second_form_result"] == 12 ? 'correct' : 'incorrect';
    $thirdResult = $_SESSION["third_form_result"] == 4 ? 'correct' : 'incorrect';
           
    echo "<div><h2>First test result: {$firstResult} : {$_SESSION["first_form_result"]}</h2></div>";
    echo "<div><h2>First test result: {$secondResult} : {$_SESSION["second_form_result"]}</h2></div>";
    echo "<div><h2>First test result: {$thirdResult} : {$_SESSION["third_form_result"]}</h2></div>";
    
    unset($_SESSION['first_form_result']);
    unset($_SESSION['second_form_result']);
    unset($_SESSION['third_form_result']);

    echo "<div><a href='task4.10.php'>Try once more</a></div>";
?>
