<?php

    // Artsiom Mkrtychan
    /* 8.02.2017 */

    session_start();
    $_SESSION["first_form_result"] = $_GET["first_form_result"];    
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body> 
    <form action="task4.10.3.php">
        <div>
            <h2>7 + 5 = ?</h2>
            <input type="radio" name="second_form_result" value="2" checked> 2<br>
            <input type="radio" name="second_form_result" value="7"> 7<br>
            <input type="radio" name="second_form_result" value="12"> 12
        </div>
        <button type="submit">Send</button>
    </form>
</body>

</html>
