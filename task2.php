<?php
    // Artsiom Mkrtychan
    //13.01.2017
    //Task2

    //3.1
    echo "<h3>3.1</h3>";
    $a = 152;
    $b = '152';
    $c = 'London';
    $d = array(152);
    $e = 15.2;
    $f = false;
    $g = true;

    $aType = gettype($a);
    $bType = gettype($b);
    $cType = gettype($c);
    $dType = gettype($d);
    $eType = gettype($e);
    $fType = gettype($f);
    $gType = gettype($g);


    echo "<div>a type : $aType</div>";
    echo "<div>b type : $bType</div>";
    echo "<div>c type : $cType</div>";
    echo "<div>d type : $dType</div>";
    echo "<div>e type : $eType</div>";
    echo "<div>f type : $fType</div>";
    echo "<div>g type : $gType</div><br/>";

    //3.2
    echo "<h3>3.2</h3>";
    $a = 10;
    $b = 5;

    $strWithReplacedVarsSingleQuotes = $b . ' из ' . $a . '-ти студентов посетили лекцию';
    $strWithReplacedVarsDoubleQuotes = "{$b} из {$a}-ти студентов посетили лекцию";

    echo "<div>Single quotes: {$strWithReplacedVarsSingleQuotes}</div>";
    echo "<div>Double quotes: {$strWithReplacedVarsDoubleQuotes}</div><br/>";

    //3.3
    echo "<h3>3.3</h3>";
    $goodMorning = "Доброе утро";
    $ladies = "дамы";
    $gentlemen = " и господа";

    echo "<div>{$goodMorning}</div>";
    echo "<div>{$ladies}</div>";
    echo "<div>{$gentlemen}</div><br/>";


    $comma = ", ";
    $concatedStr =  $goodMorning . $comma . $ladies . $gentlemen;

    echo "<div>{$concatedStr}</div>";

    //3.4
    echo "<h3>3.4</h3>";
    $firstArray = [1,2,3,4,5];
    $secondArray = [1,2,3,4,5];

    $firstArray["element"] = "element of array";

    unset($secondArray[0]);

    echo "<div>Second elem from first array : {$firstArray[2]}</div>";
    echo "<div>Second elem from second array : {$secondArray[2]}</div><br/>";

    echo "<h4>First array content:</h4>";
    echo "<pre>";
        print_r($firstArray);
    echo "</pre>";

    echo "<h4>Second array content:</h4>";
    echo "<pre>";
        print_r($secondArray);
    echo "</pre><br/>";

    $firstArraySize = count($firstArray);
    $secondArraySize = count($secondArray);

    echo "<div>First array size : {$firstArraySize}</div>";
    echo "<div>First array size : {$secondArraySize}</div>";

    //4.1
    echo "<h3>4.1</h3>";

    define("MIN", 10);
    define("MAX", 50);

    function isVarInRange($xTemp) {
        $result = "";
        if(($xTemp > MIN ) && ($xTemp < MAX)){
            $result = "+";
        } elseif (($xTemp < MIN) || ($xTemp > MAX)){
            $result = "-";
        } else {
            $result = "+-";
        }

        return $result;
    }

    $x = 25;
    $ifXInRange = isVarInRange($x);
    echo "<div>If x = 10 in range [10,50] : {$ifXInRange}</div>";

    $x = 7;
    $ifXInRange = isVarInRange($x);
    echo "<div>If x = 7 in range [10,50] : {$ifXInRange}</div>";

    $x = 50;
    $ifXInRange = isVarInRange($x);
    echo "<div>If x = 50 in range [10,50] : {$ifXInRange}</div>";



    //4.2
    echo "<h3>4.2</h3>";

    function getDiscriminant($a, $b, $c) {
        $result = pow($b,2) - 4 * $a * $c;

        return $result;
    }

    function getFirstRoot($a,$b,$d) {
        $result = (-$b + sqrt($d)) / 2 * $a;

        return $result;
    }

    function getSecondRoot($a,$b,$d) {
        $result = (-$b - sqrt($d)) / 2 * $a;

        return $result;
    }



    function printVars($a,$b,$c,$d){
        echo "<div>а : {$a}</div>";
        echo "<div>b : {$b}</div>";
        echo "<div>c : {$c}</div>";

        echo "<div>Discriminant : {$d}</div>";
    }

    function printRoots($a, $b, $d) {
        if ($d > 0 ){
            $firstRoot = getFirstRoot($a,$b,$d);
            $secondRoot = getSecondRoot($a,$b,$d);

            echo "<div>First root: {$firstRoot}</div>";
            echo "<div>Second root: {$secondRoot}</div>";
        } elseif ($d == 0){
            $firstRoot = getFirstRoot($a,$b,$d);

            echo "<div>First root: {$firstRoot}</div>";
        } else {
            echo "<div>Нет корней</div>";
        }
        echo "<br/>";
    }


    $a = 1;
    $b = -70;
    $c = 600;
    $d = getDiscriminant($a,$b,$c);

    printVars($a,$b,$c,$d);
    printRoots($a,$b,$d);


    $a = 3;
    $b = -18;
    $c = 27;
    $d = getDiscriminant($a,$b,$c);

    printVars($a,$b,$c,$d);
    printRoots($a,$b,$d);


    $a = 1;
    $b = -3;
    $c = 4;
    $d = getDiscriminant($a,$b,$c);

    printVars($a,$b,$c,$d);
    printRoots($a,$b,$d);


    //4.3
    echo "<h3>4.3</h3>";

    function mid($a, $b, $c) {

        if (($a == $b) ||
            ($a == $c)  ||
            ($b == $c)){
            echo "<div>Ошибка</div><br/>";
            return;
        }

        $result = "";

        if((($a > $b) && ($a < $c)) ||
            (($a > $c) && ($a < $b))) {

            $result = "<div>Среднее число a : {$a}</div>";
        } elseif ((($b > $a) && ($b < $c)) ||
                  (($b > $c) && ($b < $a))) {

            $result = "<div>Среднее число b : {$b}</div>";
        } else {

            $result = "<div>Среднее число c : {$c}</div>";
        }

        echo "{$result}<br/>";
    }

    function printNumbers($a,$b,$c){
        echo "<div>a : {$a}</div>";
        echo "<div>b : {$b}</div>";
        echo "<div>c : {$c}</div>";
    }

    $a = 2;
    $b = 4;
    $c = 3;

    printNumbers($a,$b,$c);
    mid($a,$b,$c);


    $a = 4;
    $b = 3;
    $c = 2;

    printNumbers($a,$b,$c);
    mid($a,$b,$c);


    $a = 3;
    $b = 2;
    $c = 4;

    printNumbers($a,$b,$c);
    mid($a,$b,$c);


    //5.1
    echo "<h3>5.1</h3>";

    $sum = 0;

    for ($i = 1; $i <= 25; $i++){
        $sum += $i;
    }

    echo "<div>Sum with for : {$sum}</div>";


    $sum = 0;
    $i = 1;

    while ($i <= 25){
        $sum += $i;
        $i++;
    }

    echo "<div>Sum with while : {$sum}</div><br/>";


    //5.2
    echo "<h3>5.2</h3>";

    $n = 10;
    $i = 1;

    while ($i < $n) {
        $iPow2 = pow($i,2);

        echo "<div>{$i} pow 2 : {$iPow2}</div>";

        $i++;
    }


    //5.2
    echo "<h3>5.3</h3>";

    $buttons = [
        "Кнопка 10",
        "Кнопка 9",
        "Кнопка 8",
        "Кнопка 7",
        "Кнопка 6",
        "Кнопка 5",
        "Кнопка 4",
        "Кнопка 3",
        "Кнопка 2",
        "Кнопка 1",
    ];

    natsort($buttons);

    echo "<ul>";
    foreach ($buttons as $button){
        echo "<li><a href=\"#\">{$button}</a></li>";
    }
    echo "</ul>";

?>

