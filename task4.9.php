<?php

// Artsiom Mkrtychan
/* 8.02.2017 */


$cookieVisite = "visited";
$cookieVisiteValue = true;

$cookieVisiteDateTime = "visiteDate";
$cookieVisiteCounter = "visiteCounter";

if(!isset($_COOKIE[$cookieVisite])) {    
    //9.1
    setcookie($cookieVisite, $cookieVisiteValue);    
    
    //9.2
    $date = new DateTime();    
    setcookie($cookieVisiteDateTime, $date->format('Y-m-d\TH:i:s'));
    
    //9.3
    setcookie($cookieVisiteCounter, 1);
        
} else {
    //9.3
    $visitsCount = $_COOKIE[$cookieVisiteCounter];    
    setcookie($cookieVisiteCounter, ++$visitsCount);
}

echo "<h3>9.1</h3>";
if(!isset($_COOKIE[$cookieVisite])) {
    //9.1
    echo "<div>Добро пожаловать, новичок!</div>";    
} else {
    //9.1
    echo "<div>С возвращением, дружище!</div>";      
    
    //9.2
    echo "<h3>9.2</h3>";
    echo "Was at: {$_COOKIE[$cookieVisiteDateTime]}";
    
    //9.3
    echo "<h3>9.3</h3>";
    echo "Visits count: {$_COOKIE[$cookieVisiteCounter]}";    
}

?>
