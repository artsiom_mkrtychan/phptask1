<?php
    //1
    // Artsiom Mkrtychan
    /* 11.01.2017 */
    echo "<h1>Hello World</h1>";

    //2
    $channelName = "Discovery";
    $manufacturerAddress = "World street, 1";
    $carColor = "Red";
    $waterTemperature = "15 degree";
    $phoneModel = "Iphone";

    //3
    $firstVariable = 3;
    $secondVariable = 5;
    $thirdVariable = 8;

    echo "<div>FirstVar: $firstVariable</div>";
    echo "<div>SecondVar: $secondVariable</div>";
    echo "<div>ThirdVar: $thirdVariable</div>";

    $sumOf3Vars = $firstVariable + $secondVariable + $thirdVariable;
    echo "<h4>Sum of 3 vars: $sumOf3Vars</h4>";

    $result = 2 + 6 + 2 / 5 - 1;
    echo "<div>Result: $result</div>";

    //4
    $a = "a var";
    $b = "b var";

    echo "<div>a: $a</div>";
    echo "<div>b: $b</div><br/>";

    $c = $a;
    $d = &$b;

    echo "<div>c: $c</div>";
    echo "<div>d: $d</div><br/>";

    $a = 3;
    $b = 4;

    echo "<div>a: $a</div>";
    echo "<div>b: $b</div>";
    echo "<div>c: $c</div>";
    echo "<div>d: $d</div>";

    //5
    define("FIRST_CONST", 41);
    define("SECOND_CONST", 33);

    $constsSum = FIRST_CONST + SECOND_CONST;

    echo "<div>Constants summ: $constsSum</div>";

    /*
    Error message:Expression is not assignable: Constant reference
    FIRST_CONST = 13;
    */
?>
