-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.34 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for news_db
CREATE DATABASE IF NOT EXISTS `news_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `news_db`;

-- Dumping structure for table news_db.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table news_db.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`) VALUES
	(1, 'sport'),
	(2, 'science'),
	(3, 'technology'),
	(4, 'busines'),
	(5, 'health');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table news_db.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `header` varchar(100) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;

-- Dumping data for table news_db.news: ~29 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `date`, `header`, `content`, `category_id`, `user_id`, `status`) VALUES
	(2, '2017-01-01', 'Gonzalo Higuain\'s 16th league goal of the season helped send Juventus seven points clear at the top ', 'Crotone, second from bottom, held the leaders for an hour before Mario Mandzukic scored from close range after Kwadwo Asamoah\'s header was dropped.\r\n\r\nHiguain\'s composed finish from Tomas Rincon\'s pass doubled the lead.\r\n\r\nThird-placed Napoli can cut Juve\'s lead to six points if they beat Genoa on Friday (19:45 GMT).\r\n\r\nJuve are looking to become champions of Italy for a sixth successive season.\r\n\r\nThey were denied a third goal when former Roma midfielder Miralem Pjanic hit the bar five minutes from time.\r\n\r\nMassimiliano Allegri\'s side are next in action on Sunday when they face Cagliari, 15th in the table, in Sardinia (19:45 GMT).\r\n\r\nIn Wednesday\'s other game, AC Milan ended with nine men but beat Bologna 1-0 away from home.\r\n\r\nGabriel Paletta and Juraj Kucka were both dismissed before Mario Pasalic\'s 89th-minute winner.', 1, 1, 1),
	(3, '2017-01-09', 'Quinoa genome could see \'super-food\' prices tumble', 'Experts say that quinoa was first domesticated more than 7,000 years ago around Lake Titicaca in the Andean highlands and centuries later became known as the "mother grain" of the Inca empire.\r\nAfter the arrival of the Spanish, the crop was marginalised and never fully domesticated or bred to its full potential.\r\nThe crop grows at high altitudes and in cool temperatures, factors which have limited its production outside of Peru, Bolivia and Ecuador, although many countries grow small amounts.\r\nHowever the nutritional composition and the fact that quinoa is gluten-free, high in protein and moderate in carbohydrate has seen international demand for the foodstuff soar.\r\nPrices tripled between 2006 and 2013 principally because quinoa\'s adoption as a "super-food" in Europe and the US.', 2, 2, 0),
	(9, '2017-01-14', 'Twitter rolls out new anti-abuse tools', 'It comes in the wake of heavy criticism about harassment on its platform and a failure to find a buyer after months of rumours about takeovers.\r\nIt has announced three main changes, which will be rolled out in the "coming weeks".\r\nIt includes moves to identify people who have been permanently suspended and stop them creating new accounts.', 3, 3, 1),
	(10, '2017-01-24', 'Bank warns \'lax financial rules\' are a route to failure', 'Sir Jon Cunliffe told the BBC that "lax controls" risked undoing progress that had been made since the financial crisis.\r\n"We\'ve made very substantial progress since the financial crisis, increasing the resilience of the financial sector and increasing its ability to support the economy in times of stress both nationally and in Europe and globally, including the US," Sir Jon told me.\r\n"Those changes were necessary.\r\n"None of us want to see again the sorts of events we saw between 2007 and 2009 and the costs of those events are still very clear.\r\n"In order to have a resilient financial sector and consistent regulation internationally we need international standards, we need the reforms we have had and it is important we preserve them."\r\nSir Jon\'s comments come after suggestions that if Britain did not secure a good trade deal with the European Union following Brexit, the UK could become an offshore tax haven - encouraging businesses and banks to move to the country to avoid tougher regulations elsewhere.\r\nDonald Trump, via an executive order, has also announced there will be a review of the Dodd-Frank legislation in America.', 4, 4, 1),
	(12, '2017-02-09', 'Male contraceptive gel passes monkey test', 'Allan Pacey, professor of andrology at the University of Sheffield, said: "The study shows that, in adult male monkeys at least, the gel is an effective form of contraception.', 5, 5, 0),
	(155, '2017-02-14', 'The Body Shop: What went wrong?', 'The 42-year-old stage manager used to be a regular \r\n            shopper at the High Street chain, but now she tends to go to Boots instead.\r\n            And shes not alone. According to a report in the Financial \r\n            Times, its owner, cosmetics giant LOreal, plans to offload the \r\n            High Street chain due to its slowing sales.', 1, 1, 1),
	(156, '2017-02-23', 'Where do unicorns go to university?', 'Harvard, the worlds wealthiest university, \r\n            is in second place, with US institutions taking \r\n            nine of the top 15 places, with many of these entries \r\n            dominated by tech start-ups. \r\n            The first non-US university is in fourth place with a combined figure for all \r\n            the Indian Institutes of Technology.\r\n            This network of 23 institutions across India has a notoriously competitive \r\n            entrance exam - often claimed to be the most oversubscribed applications process in the world.', 2, 2, 0),
	(157, '2017-02-28', 'What really happened when Swedes tried six-hour days?', 'Designed to measure well-being in a sector thats struggling \r\n            to recruit enough staff to care for the countrys ageing population, \r\n            extra nurses were brought in to cover the lost hours.\r\n            The projects independent researchers were also paid to study \r\n            employees at a similar care home who continued to work regular days.\r\n            Their final report is due out next month, but data released so \r\n            far strongly backs Ms Telanders arguments.', 3, 3, 1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table news_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `second_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8;

-- Dumping data for table news_db.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `login`, `password`, `first_name`, `second_name`) VALUES
	(1, 'johnlogin', '12345678', 'Владислав', 'larrison'),
	(2, 'lisalogin', '12345678', 'Елена', 'jecings'),
	(3, 'katelogin', '12345678', 'Тимофей', 'Опель'),
	(4, 'samlogin', '12345678', 'sam', 'lesge'),
	(5, 'traislogin', '12345678', 'Елена', 'treees'),
	(407, 'Johnlogin', '12345678', 'Джон', 'Смит'),
	(410, 'Johnlogin', '12345678', 'Джон', 'Смит');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
