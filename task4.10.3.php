<?php

    // Artsiom Mkrtychan
    /* 8.02.2017 */

    session_start();
    $_SESSION["second_form_result"] = $_GET["second_form_result"];   
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>    
    <form action="task4.10.4.php">
        <div>
            <h2>3 + 1 = ?</h2>
            <input type="radio" name="third_form_result" value="4" checked> 4<br>
            <input type="radio" name="third_form_result" value="9"> 9<br>
            <input type="radio" name="third_form_result" value="14"> 14
        </div>
        <button type="submit">Get result</button>
    </form>
</body>

</html>
