<?php

// Artsiom Mkrtychan
/* 8.02.2017 */

//13.1
echo "<h3>13.1</h3>";
class Car 
{
    const FIRST_CONSTANT = 2;
    const SECOND_CONSTANT = 5;
    const THIRD_CONSTANT = 12;
    
    var $fuel = 10;
    var $doorsCount;
    var $price;    
    
    function __construct($fuel, $doorsCount, $price) 
    { 
        $this->fuel = $fuel;
        $this->doorsCount = $doorsCount;
        $this->price = $price;    
    } 
    
    public function fuelСonsumption($path)
    {
        return $path/100*$fuel;
    }
    
    public static function getConstants() {
        $oClass = new ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
    
    public static function getMaxConstant(){
        $maxConst = PHP_INT_MIN ;
        $constsArray = Car::getConstants();
        
        foreach($constsArray as $key => $value){
            if($maxConst < $value){
                $maxConst = $value;
            }
        }
        
        return $maxConst;
    }
    
    public static function printConstants(){     
        $constsArray = Car::getConstants();
        
        foreach($constsArray as $key => $value){
           echo "<div>{$key}:{$value}</div>";
        }   
        echo "<br/>";
    }
    
}

$car1 = new Car(100,7,15000);
$car2 = new Car(200,3,27000);

$car1->price = 5000;
$car2->price = 10000;

$car3 = new Car(300,3,15000);
$car4 = new Car(250,5,25000);

Car::printConstants();

$maxConst = Car::getMaxConstant();
echo "Max constant is: {$maxConst}";


//14.1
echo "<h3>14.1</h3>";

class Figure 
{       
    const SIDES_COUNT = 0;
    
    protected $area = 0;
    protected $color;
        
    public function infoAbout(){
        return "Это геометрическая фигура!";
    }    
}

class Rectangle extends Figure
{       
    const SIDES_COUNT = 4;
    
    private $a;
    private $b;
       
    function __construct($a, $b) 
    { 
        $this->a = $a;
        $this->b = $b;       
    } 
    
    public function getArea(){
        $this->area = $this->a * $this->b;
        return $this->area;
    }  
    
    public final function infoAbout(){
        return "Это прямоугольник. У него 4 стороны";
    }   
}

class Triangle extends Figure
{       
    const SIDES_COUNT = 3;
    
    private $a;
    private $b;
    private $c;
    
    function __construct($a, $b, $c) 
    { 
        $this->a = $a;
        $this->b = $b;       
        $this->c = $c;       
    } 
    
    public function getArea(){
        $p = ($this->a + $this->b + $this->c) / 2;
        $this->area = sqrt($p * ($p - $this->a) * ($p - $this->b) * ($p - $this->c));
        return $this->area;
    } 
    
    public final function infoAbout(){
        return "Это треугольник. У него 3 стороны";
    }  
}

class Square extends Figure
{       
    const SIDES_COUNT = 4;
    
    private $a;
   
    function __construct($a) 
    { 
        $this->a = $a;   
    } 
    
    public function getArea(){
        $this->area = $this->a * $this->a;
        return $this->area;
    } 
    
    public final function infoAbout(){
        return "Это квадрат. У него 4 стороны";
    }  
}

function printArea($figure)
{    
    echo "<div>Figure:{$figure->infoAbout()}</div>";
    echo "<div>Area:{$figure->getArea()}</div><br/>";
}


$rectangle1 = new Rectangle(2,4);
$rectangle2 = new Rectangle(4,8);

$triangle1 = new Triangle(4,8,9);
$triangle2 = new Triangle(8,16,18);

$square1 = new Square(4);
$square2 = new Square(8);

printArea($rectangle1);
printArea($rectangle2);

printArea($triangle1);
printArea($triangle2);

printArea($square1);
printArea($square2);


//15.1
echo "<h3>15.1</h3>";

class Book
{  
    const PDF_BOOK = "bookPdf";
    const DOC_BOOK = "bookDoc";
    const TXT_BOOK = "bookTxt";
    
    private $author;
    private $name;
    private $description;

    function __construct($author,$name,$description)
    { 
        $this->author = $author;   
        $this->name = $name;
        $this->description = $description;
    } 
        
    public function getAuthor()
    {        
        return $this->author;
    } 
    
    public function setAuthor($author)
    {        
        $this->author = $author;
    } 
    
    public function getName()
    {        
        return $this->name;
    } 
    
    public function setName($name)
    {        
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }


    
    public function printBook()
    {    
         echo 
            '<div>
                <p>                    
                    <a href="files/'.$this->getDescription() .'">'. $this->getAuthor(). ', '. $this->getName(). '</a>
                </p>
            </div>';
    }
}

interface Iconed 
{
    public function getIcon();
}

class PdfBook extends Book implements Iconed
{
    private $icon = "images/bookPdf.png";
    
    public function getIcon()
    {
        return $this->icon;
    }
    
    public function printBook()
    {    
         echo 
            '<div>
                <p> 
                    <img src="'. $this->getIcon(). '" style="height: 25px; width: 25px;" alt="" />
                    <a href="files/'.$this->getDescription() .'" style="text-decoration: none; color : black;">'. $this->getAuthor(). ', '. $this->getName(). '</a>
                </p>
            </div>';
    }
}

class DocBook extends Book implements Iconed
{
    private $icon = "images/bookDoc.png";
    
    public function getIcon()
    {        
        return $this->icon;
    }   
    
    public function printBook()
    {    
        echo 
            '<div>
                <p> 
                    <img src="'. $this->getIcon(). '" style="height: 25px; width: 25px;" alt="" />
                    <a href="files/'.$this->getDescription() .'" style="text-decoration: none; color : black;">'. $this->getAuthor(). ', '. $this->getName(). '</a>
                </p>
            </div>';
    }
}

class TxtBook extends Book implements Iconed
{
    private $icon = "images/bookTxt.png";
        
    public function getIcon()
    {        
        return $this->icon;
    }  
    public function printBook()
    {    
    echo 
        '<div>
            <p> 
                <img src="'. $this->getIcon(). '" style="height: 25px; width: 25px;" alt="" />
                <a href="files/'.$this->getDescription() .'"  style="text-decoration: none; color : black;">'. $this->getAuthor(). ', '. $this->getName(). '</a>
            </p>
        </div>';
    }
}

class DbHelper
{
    private $_connection;
	private static $_instance; 
	private $_host = "localhost";
	private $_username = "root";
	private $_password = "";
	private $_database = "books_db";
	    
	public static function getInstance() 
    {
		if(!self::$_instance) { 
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	    
	private function __construct() 
    {
		$this->_connection = new mysqli($this->_host, $this->_username, 
			$this->_password, $this->_database);
			
		if(mysqli_connect_error()) {
			trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
				 E_USER_ERROR);
		}
	}
	
	private function __clone() {}

	public function getConnection() 
    {
		return $this->_connection;
	}   
}

class BookFactory
{    
    public static function getBook($resultBookRow)
    {
        $bookType = $resultBookRow['type'];
        $book = new Book("","","");
        switch($bookType){
            case Book::PDF_BOOK:
                $book = new PdfBook($resultBookRow['author'],$resultBookRow['name'],$resultBookRow['description']);
                break;  
            case Book::DOC_BOOK:
                $book = new DocBook($resultBookRow['author'],$resultBookRow['name'],$resultBookRow['description']);
                break;  
            case Book::TXT_BOOK:
                $book = new TxtBook($resultBookRow['author'],$resultBookRow['name'],$resultBookRow['description']);
                break;                  
        }  
        
        return $book;     
    }    
}



$dbHelper = DbHelper::getInstance();   

$sqlGetAllBooks = "SELECT * FROM books";  

$connection = $dbHelper->getConnection();

$booksResult = $connection->query($sqlGetAllBooks);

if ($booksResult->num_rows > 0) {     
    while($resultBookRow = $booksResult->fetch_assoc()) {
        $book = BookFactory::getBook($resultBookRow);
        $book->printBook();
    }
} else {
    echo "No books";
}

?>
