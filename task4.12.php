<?php

// Artsiom Mkrtychan
/* 8.02.2017 */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "news_db";

$mysqli = new mysqli($servername, $username, $password, $dbname);

/* check connection */ 
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}


//12.1
echo "<h3>12.1</h3>";

$sqlAddNews = "
    INSERT INTO news
        (date,header,content,category_id,user_id,status)
    VALUES
        (
            '2017-02-14',
            'The Body Shop: What went wrong?',
            'The 42-year-old stage manager used to be a regular 
            shopper at the High Street chain, but now she tends to go to Boots instead.
            And shes not alone. According to a report in the Financial 
            Times, its owner, cosmetics giant LOreal, plans to offload the 
            High Street chain due to its slowing sales.',
            1,
            1,
            1
        ),
        (
            '2017-02-23',
            'Where do unicorns go to university?',
            'Harvard, the worlds wealthiest university, 
            is in second place, with US institutions taking 
            nine of the top 15 places, with many of these entries 
            dominated by tech start-ups. 
            The first non-US university is in fourth place with a combined figure for all 
            the Indian Institutes of Technology.
            This network of 23 institutions across India has a notoriously competitive 
            entrance exam - often claimed to be the most oversubscribed applications process in the world.',
            2,
            2,
            0
        ),
        (
            '2017-02-28',
            'What really happened when Swedes tried six-hour days?',
            'Designed to measure well-being in a sector thats struggling 
            to recruit enough staff to care for the countrys ageing population, 
            extra nurses were brought in to cover the lost hours.
            The projects independent researchers were also paid to study 
            employees at a similar care home who continued to work regular days.
            Their final report is due out next month, but data released so 
            far strongly backs Ms Telanders arguments.',
            3,
            3,
            1
        );
";

if (mysqli_query($mysqli, $sqlAddNews)) {
    echo "New news created successfully";
} else {
    echo "Error: " . $sqlAddNews . "<br>" . mysqli_error($mysqli);
}


//12.2
echo "<h3>12.2</h3>";

function printUsers($result)
{
    if ($result->num_rows > 0) {   
        echo "<div>Users:</div><br/>"; 
        
        while($row = $result->fetch_assoc()) {
            echo 
                "<div>".
                    "id: " . $row["id"]. 
                    " - login: " . $row["login"].
                    " - password: " . $row["password"].
                    " - first_name: " . $row["first_name"].
                    " - second_name: " . $row["second_name"].
                "</div><br/>";
        }
    } else {
        echo "0 results";
    }
}

function printNews($result)
{
    if ($result->num_rows > 0) {    
        echo "<div>News:</div><br/>";

        while($row = $result->fetch_assoc()) {
            echo 
                "<div>".
                    " - id: " . $row["id"]. 
                    "<br/> - date: " . $row["date"].
                    "<br/> - header: " . $row["header"].
                    "<br/> - content: <br/>" . $row["content"].
                    "<br/> - category_id: " . $row["category_id"].
                    "<br/> - user_id: " . $row["user_id"].
                    "<br/> - status: " . $row["status"].
                "</div><br/>";
        }
    } else {
        echo "0 results";
    }
}

function printNewsCount($result)
{
    if ($result->num_rows > 0) {         
        while($row = $result->fetch_assoc()) {
            echo 
                "<div>".
                    "news_count: " . $row["news_count"].               
                "</div>";
        }
    } else {
        echo "0 results";
    }
}

function printNewsPhp($result)
{
    if ($result->num_rows > 0) {         
        while($row = $result->fetch_assoc()) {
            echo 
                "<div>".                        
                    "<h1>".$row["header"]."</h1>".
                    "<p>".substr($row["content"], 0, 100)."</p>".
                    "<a href='/news/view/".$row["id"]."'>Читать далее </a>".
                "</div>";
        }
    } else {
        echo "0 results";
    }
}



$sqlDeleteWhere = "DELETE FROM users WHERE first_name='Томас'";
$sqlDeleteAnd = "DELETE FROM users WHERE first_name='Томас' AND second_name='Смит'";
$sqlDeleteOr = "DELETE FROM users WHERE first_name='Томас' OR first_name='Джон'";

$sqlSelectUsers = "SELECT * FROM users";

$sqlInsertUsers = " 
    INSERT INTO users
        (login,password,first_name,second_name)
    VALUES
    (
        'Tomaslogin','12345678','Томас','Лопатин'
    ),
    (
        'Johnlogin','12345678','Джон','Смит'
    ),
    (
        'Tomaslogin','12345678','Томас','Смит'
    )
    ";


//delete or
echo "<br/><h2>delete or</h2>";

if (mysqli_query($mysqli, $sqlInsertUsers)) {
    echo "New users created successfully";
} else {
    echo "Error: " . $sqlInsertUsers . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

if (mysqli_query($mysqli, $sqlDeleteOr)) {
    echo "Delete Томас или Джон successfully";
} else {
    echo "Error: " . $sqlDeleteWhere . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

//delete and
echo "<br/><h2>delete and</h2>";

if (mysqli_query($mysqli, $sqlInsertUsers)) {
    echo "New users created successfully";
} else {
    echo "Error: " . $sqlInsertUsers . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

if (mysqli_query($mysqli, $sqlDeleteAnd)) {
    echo "Delete Томас и Джон successfully";
} else {
    echo "Error: " . $sqlDeleteAnd . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

//delete where
echo "<br/><h2>delete where</h2>";

if (mysqli_query($mysqli, $sqlInsertUsers)) {
    echo "New users created successfully";
} else {
    echo "Error: " . $sqlInsertUsers . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

if (mysqli_query($mysqli, $sqlDeleteWhere)) {
    echo "Delete Томас successfully";
} else {
    echo "Error: " . $sqlDeleteWhere . "<br>" . mysqli_error($mysqli);
}

printUsers(mysqli_query($mysqli, $sqlSelectUsers));


//12.3
echo "<h3>12.3</h3>";

//Updated user to тимофей опель 
$sqlUpdateNameFamily = "UPDATE users SET first_name='Тимофей', second_name='Опель' WHERE id=3";

printUsers(mysqli_query($mysqli, $sqlSelectUsers));

if (mysqli_query($mysqli, $sqlUpdateNameFamily)) {
    echo "Updated user to тимофей опель successfully<br>";
} else {
    echo "Error: " . $sqlUpdateNameFamily . "<br>" . mysqli_error($mysqli);
}

//Updated 3 users with greatest id to смит
printUsers(mysqli_query($mysqli, $sqlSelectUsers));

$sqlSelect3UserWithLargestId = "SELECT id FROM users ORDER BY id DESC  LIMIT 3";
$sqlUpdateNameSmith = "UPDATE users SET second_name='Смит' WHERE id = ";

$result = mysqli_query($mysqli, $sqlSelect3UserWithLargestId);

if ($result->num_rows > 0) {         
    while($row = $result->fetch_assoc()) {
        
        if (mysqli_query($mysqli, $sqlUpdateNameSmith . $row["id"])) {
            echo "Updated 3 users with greatest id to смит successfully";
        } else {
            echo "Error: " . $sqlUpdateNameSmith . $row["id"] . "<br>" . mysqli_error($mysqli);
        }              
    }
} else {
    echo "Updated 3 users with greatest id to смит is not successfully";
    echo "Error: " . $sqlUpdateNameSmith . "<br>" . mysqli_error($mysqli);
}



printUsers(mysqli_query($mysqli, $sqlSelectUsers));



//12.4
echo "<h3>12.4</h3>";

//3 last new with category_id = 2
$sqlSelectNews = "SELECT * FROM news;";
$sqlSelectLastNewsWithCatId2 = "SELECT * FROM news WHERE category_id=2 ORDER BY id DESC LIMIT 3;" ;

echo "<br/><div><h4>All news</h4></div>";
printNews(mysqli_query($mysqli, $sqlSelectNews));

echo "<br/><div><h4>Last 3 news with category_id = 2</h4></div>";
printNews(mysqli_query($mysqli, $sqlSelectLastNewsWithCatId2));

//all users with name владислав and елена  
$sqlUsersElenaAndVladislau = "SELECT * FROM users WHERE first_name='Владислав' OR first_name='Елена';";

printUsers(mysqli_query($mysqli, $sqlUsersElenaAndVladislau));

//status count 
$sqlNewsStatus0 = "SELECT COUNT(id) AS news_count FROM news WHERE status=0";
$sqlNewsStatus1 = "SELECT COUNT(id) AS news_count FROM news WHERE status=1";
$sqlNewsCount = "SELECT COUNT(*) AS news_count FROM news";

echo "<div><h4>News with status 0:</h4></div>";
printNewsCount(mysqli_query($mysqli, $sqlNewsStatus0));

echo "<div><h4>News with status 1:</h4></div>";
printNewsCount(mysqli_query($mysqli, $sqlNewsStatus1));

echo "<div><h4>News count:</h4></div>";
printNewsCount(mysqli_query($mysqli, $sqlNewsCount));


//12.5
echo "<h3>12.5</h3>";

//print all news with status 1 via php 
$sqlNewsWithStatus1 = "SELECT * FROM news WHERE status=1";

printNewsPhp(mysqli_query($mysqli, $sqlNewsWithStatus1));

/* close connection */
$mysqli->close();

?>
